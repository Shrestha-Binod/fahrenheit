package sheridan;

import java.util.Scanner;

public class Fahrenheit {

	public static int convertFromFahrenheit(int temperature) {
		int f = temperature * (9 / 5) + 32;

		return f;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = 	new Scanner(System.in);
		System.out.println("Enter the temperature for conversion: ");
		int c = scan.nextInt();
		int temp = convertFromFahrenheit(c);
		System.out.println(temp + "f");

	}

}
